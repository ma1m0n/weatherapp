# Weather App
Using on the server side Node.js with Express.js, and the client-side using Vue.js.

Crawling the Weather Data From https://darksky.net/forecast/
and use Google Maps Geocoding API for the autocomplete input.

Enjoy :)

## Usage

### Installation

Install the dependencies

```sh
$ npm install
```
Run app

```sh
$ npm start
```

## App Info

### Author

Gal Maimon