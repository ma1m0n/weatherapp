class WeatherData {
  constructor() {
    this.temperature = '';
    this.humidity = '';
    this.windspeed = '';
    this.icon = '';
  }
}

module.exports = WeatherData;
