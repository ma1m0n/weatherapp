
/* eslint no-console: 0 */
// @ts-check
const express = require('express');
const cheerio = require('cheerio');
const fetch = require('node-fetch').default;
const WeatherData = require('./models/weatherData');
const Place = require('./models/location');
const path = require('path');
// const cors = require('cors');

const app = express();

// Port Number
const PORT = process.env.PORT || 8081;

// Serving static files
app.use('/static', express.static(path.join(__dirname, 'public/static')));

// Google Map API Key
const API_KEY = 'AIzaSyA6_2dCYf1UK_K8MZg1NfXp-Q_0Fyzr1ms';

// CORS Middleware
// Not needed for production on the same port
// app.use(cors());

function urlCreate(lat, lng) {
  return `https://darksky.net/forecast/${lat},${lng}/ca12/en`;
}


async function getWeather(url) {
  const weather = new WeatherData();
  try {
    const res = await fetch(url);
    const html = await res.text();
    const $ = await cheerio.load(html);
    // Start Doing Crawling
    const title = $('#title');
    const details = $('#currentDetails');
    weather.temperature = title.find('.summary.swap').text();
    weather.icon = title.find('.skycon.swip canvas').attr('class');
    weather.windspeed = details.find('#currentDetails .wind .num.swip').text();
    weather.humidity = details.find('#currentDetails .humidity .num.swip').text();
    console.log(`${Date.now()} - Weather Data Crawled.`);
  } catch (error) {
    throw error;
  }
  return weather;
}

function sigCoord(val) {
  return (Math.round(val * 10000) / 10000).toString();
}

async function getPlaces(query) {
  try {
    const resGeo = await fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${query}&key=${API_KEY}`);
    const geoJson = await resGeo.json();
    const { status, results } = geoJson;
    if (status && status === 'ZERO_RESULTS') {
      return [];
    } else if (!status || status !== 'OK') {
      throw new Error('Geo API Error!');
    }
    const places = results.map((res) => {
      const place = new Place();
      place.name = res.formatted_address;
      const loc = res.geometry.location;
      place.geo.lat = sigCoord(loc.lat);
      place.geo.lng = sigCoord(loc.lng);
      return place;
    });
    return places;
  } catch (error) {
    throw error;
  }
}

app.get('/places', async (req, res) => {
  const { q } = req.query;
  try {
    const places = await getPlaces(q);
    res.send(places);
  } catch (error) {
    res.status(500).send(error.massage);
  }
});

app.get('/weather', async (req, res) => {
  const { lat } = req.query;
  const { lng } = req.query;
  let urlReq;
  if (lat && lng) {
    urlReq = urlCreate(lat, lng);
  } else {
    res.status(500).send('Something broke!');
    return;
  }
  try {
    const resWeatherData = await getWeather(urlReq);
    res.send(resWeatherData);
  } catch (error) {
    res.status(500).send(error.massage);
  }
});

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});

app.listen(PORT, () => {
  console.log(`Let's check the weather on port ${PORT}`);
});


module.exports = app;
